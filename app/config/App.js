import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import MainPage from '../page/MainPage';
import MovieDetails from '../page/MovieDetails';

function renderMovieDetails(prop) {
  return <MovieDetails movieId={parseInt(prop.match.params.movieId, 10)} />;
}

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={MainPage} />
          <Route path="/movie/:movieId/details" render={renderMovieDetails} />
        </Switch>
      </BrowserRouter>
    );
  }
}
