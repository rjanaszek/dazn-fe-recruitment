import { getMoviesBySearchPhrase, getMovieDetails } from './moviedbService';

describe('Moviedb service', () => {
  it('should get movie list based on given search phrase', () => {
    // let's query movie db with popular search phrase
    return getMoviesBySearchPhrase('seven')
      .then((result) => {
        expect(result).toBeTruthy();
        // check if we have any results
        expect(result.results.length).toBeGreaterThan(0);
      });
  });

  it('should get movie details', () => {
    return getMovieDetails(807)
      .then((result) => {
        expect(result).toBeTruthy();
        expect(result).toHaveProperty('title');
        expect(result).toHaveProperty('overview');
        expect(result).toHaveProperty('release_date');
        expect(result).toHaveProperty('original_title');
      });
  });
});
