import { fetchClient } from './moviedbFetch';

const fetch = jest.fn();
fetch.mockReturnValue(Promise.resolve({
  json() {},
}));
window.fetch = fetch;

describe('Fetch client', () => {
  it('should issue fetch to rest api with configuration given', () => {
    const configuration = {
      search: 'unescaped&characters?',
      'input?': 'value',
    };
    fetchClient('', configuration);

    expect(fetch.mock.calls.length).toBe(1);
    // test first call if it has properly set input arguments
    expect(fetch.mock.calls[0][0]).toMatch(/search=unescaped%26characters%3F/);
    expect(fetch.mock.calls[0][0]).toMatch(/input%3F=value/);
  });
});
