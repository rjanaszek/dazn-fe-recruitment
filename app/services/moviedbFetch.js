import 'whatwg-fetch';
import apiKey from '../../config/api/apiKey.json';

const ROOT_URL = 'https://api.themoviedb.org/3';

export function fetchClient(path, queryParameters) {
  const queryConfig = {
    api_key: apiKey.apiKey,
    ...queryParameters,
  };
  const resourceUriParameters = Object.keys(queryConfig).reduce((acc, configKey, indx) => {
    let uriParameter = acc;
    if (indx > 0) {
      uriParameter += '&';
    }

    uriParameter = `${uriParameter}${encodeURIComponent(configKey)}=${encodeURIComponent(queryConfig[configKey])}`;

    return uriParameter;
  }, '?');

  return fetch(ROOT_URL + path + resourceUriParameters)
    .then((response) => {
      return response.json();
    });
}
