import { fetchClient } from './moviedbFetch';

/**
 * Search for movie by given movieId and retrieves its' data
 * @param {number} movieId
 * @returns Promise<any>
 */
export function getMovieDetails(movieId) {
  return fetchClient(`/movie/${movieId}`);
}

/**
 * Queries database with given searchPhrase
 * @param {string} searchPhrase Search phrase to be used for quering
 * @param {number} page Search page
 * @returns Promise<any>
 */
export function getMoviesBySearchPhrase(searchPhrase, page = 1) {
  return fetchClient('/search/movie', { query: searchPhrase, page });
}
