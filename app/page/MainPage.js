import React, { Component } from 'react';
import SearchController from '../controllers/SearchController';

export default class MainPage extends Component {
  render() {
    return (
      <div>
        <h2 id="heading">Welcome to moviedb!</h2>
        <SearchController />
      </div>
    );
  }
}
