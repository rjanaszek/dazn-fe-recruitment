import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MovieDetailsController from '../controllers/MovieDetailsController';

export default class MovieDetails extends Component {
  static propTypes = {
    movieId: PropTypes.number.isRequired,
  }

  render() {
    return <MovieDetailsController movieId={this.props.movieId} />;
  }
}
