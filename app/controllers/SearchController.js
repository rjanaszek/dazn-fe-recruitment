import React, { Component } from 'react';
import SearchResultList from '../components/SearchResultTable';
import { getMoviesBySearchPhrase } from '../services/moviedbService';

export default class SearchController extends Component {
  constructor() {
    super();

    this.state = {
      isLoading: false,
      searchPhrase: '',
      searchResultList: [],
      currentResultPage: 1,
      totalPages: null,
    };
  }

  onSearchPhraseChange = (event) => {
    this.setState({
      searchPhrase: event.target.value,
    });
  }

  onSearchClick = () => {
    const { searchPhrase } = this.state;
    if (!searchPhrase) {
      this.setState({
        searchResultList: [],
        currentResultPage: 1,
        totalPages: null,
      });
    } else {
      this.loadResultsBySearchPhrase(this.state.searchPhrase, 1);
    }
  }

  getNextPage = (pageNumber) => {
    this.loadResultsBySearchPhrase(this.state.searchPhrase, pageNumber);
  }

  loadResultsBySearchPhrase = (searchPhrase, pageNumber) => {
    this.setState({
      searchPhrase,
    });
    getMoviesBySearchPhrase(searchPhrase, pageNumber)
      .then((result) => {
        this.setState({
          currentResultPage: pageNumber,
          searchResultList: result.results,
          totalPages: result.total_pages,
        });
      });
  }

  render() {
    return (
      <div>
        {this.state.isLoading && <div>is still loading</div>}
        <span>Search for: </span>
        <input onChange={this.onSearchPhraseChange} value={this.state.searchPhrase} />
        <button onClick={this.onSearchClick}>Search</button>
        <SearchResultList
          data={this.state.searchResultList}
          currentPage={this.state.currentResultPage}
          totalPages={this.state.totalPages}
          getAnotherPage={this.getNextPage}
        />
      </div>
    );
  }
}
