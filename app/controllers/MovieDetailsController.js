import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getMovieDetails } from '../services/moviedbService';
import MovieDetails from '../components/MovieDetails';

export default class MovieDetailsController extends Component {
  static propTypes = {
    movieId: PropTypes.number.isRequired,
  }

  constructor() {
    super();
    this.state = {
      isLoaded: false,
    };
  }

  componentDidMount() {
    getMovieDetails(this.props.movieId)
      .then((result) => {
        const {
          title,
          original_title: originalTitle,
          production_countries: productionCountries,
          release_date: releaseDate,
          overview,
        } = result;
        this.setState({
          title,
          originalTitle,
          productionCountries,
          releaseDate,
          overview,
          isLoaded: true,
        });
      });
  }

  render() {
    if (!this.state.isLoaded) {
      return 'Loading...';
    }
    const {
      title, originalTitle, productionCountries, releaseDate, overview,
    } = this.state;
    return (<MovieDetails
      title={title}
      originalTitle={originalTitle}
      productionCountries={productionCountries}
      releaseDate={releaseDate}
      overview={overview}
    />);
  }
}
