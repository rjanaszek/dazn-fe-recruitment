import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default class MovieDetails extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
  }

  render() {
    return (
      <div>
        <Link to="/">Return to main page</Link>
        <table>
          <tbody>
            <tr>
              <th>Title</th>
              <td>{this.props.title}</td>
            </tr>
          </tbody>
        </table>
      </div>);
  }
}
