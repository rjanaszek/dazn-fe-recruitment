import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const OVERVIEW_TEXT_MAX_LENGTH = 100;

export default class SearchResultTable extends Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape).isRequired,
    currentPage: PropTypes.number,
    totalPages: PropTypes.number,
    getAnotherPage: PropTypes.func,
  }

  changePage = pageNumberModifier => (event) => {
    this.props.getAnotherPage(this.props.currentPage + pageNumberModifier);
    event.preventDefault();
  }

  renderPageSelector() {
    let prevPageSelector = '';
    let nextPageSelector = '';

    if (this.props.currentPage > 1) {
      prevPageSelector = <a href="#" onClick={this.changePage(-1)}>previous</a>;
    }

    if (this.props.currentPage < this.props.totalPages) {
      nextPageSelector = <a href="#" onClick={this.changePage(1)}>next</a>;
    }

    return (
      <span>
        {prevPageSelector} Page {this.props.currentPage} of {this.props.totalPages}
        &nbsp;{nextPageSelector}
      </span>);
  }

  render() {
    if (!this.props.data.length) {
      return <div>No results</div>;
    }

    return (
      <div>
        <table>
          <thead>
            <tr>
              <th>Title</th>
              <th>Original title</th>
              <th>Release date</th>
              <th>Overview</th>
            </tr>
          </thead>
          <tbody>
            {this.props.data.map((row) => {
                return (
                  <tr key={row.id}>
                    <td><Link to={`/movie/${row.id}/details`}>{row.title}</Link></td>
                    <td>{row.original_title}</td>
                    <td>{row.release_date}</td>
                    <td>{row.overview.length > OVERVIEW_TEXT_MAX_LENGTH ?
                      `${row.overview.substr(0, OVERVIEW_TEXT_MAX_LENGTH)}...` :
                      row.overview}
                    </td>
                  </tr>);
              })}
          </tbody>
        </table>
        {this.renderPageSelector()}
      </div>);
  }
}
